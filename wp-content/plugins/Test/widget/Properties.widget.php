<?php 
	
	
	class Properties_Widget extends WP_Widget
	{
		
		/**
		 * Constructor
		 *
		 * @return void
		 * @since 0.5.0
		 */
		function Properties_Widget()
		{
			parent::__construct( false, __("Property types", TEST), array('description' => __("Pult of Magic manipulations", TEST), 'classname' => 'smco_my_consume') );
		}
		
		/**
		 * Display widget.
		 *
		 * @param array $args
		 * @param array $instance
		 * @return void
		 * @since 0.5.0
		 */
		function widget( $args, $instance ) 
		{
			global $Ermak_Consume;
			extract( $args );
			$instance['title'] ? NULL : $instance['title'] = '';
			$title 			= apply_filters('widget_title', $instance['title']);
			$output 		= $before_widget."\n";
			if($title)
				$output 	.= $before_title.$title.$after_title;
			$user_id		= get_current_user_id();
			
			$output 		.= "<ul>";
			$terms = get_terms( array(
				'taxonomy'      => array( PROPERTY_TYPE ),
				'orderby'       => 'title', 
				'order'         => 'ASC',
				'hide_empty'    => true, 
				'object_ids'    => null, // 
				'include'       => array(),
				'exclude'       => array(), 
				'exclude_tree'  => array(), 
				'number'        => '', 
				'fields'        => 'all', 
				'count'         => false,
				'slug'          => '', 
				'parent'         => '',
				'hierarchical'  => true, 
				'child_of'      => 0, 
				'get'           => '', 
				'name__like'    => '',
				'pad_counts'    => false, 
				'offset'        => '', 
				'search'        => '', 
				'cache_domain'  => 'core',
				'name'          => '', 
				'childless'     => false, 
				'update_term_meta_cache' => true,
				'meta_query'    => '',
			) );

			foreach( $terms as $term )
			{
				$output 		.= "<li><a  href='" . get_term_link($term->term_id) ."'>" . $term->name . "</a></li>";
			}
			$output 		.= "</ul>";
			$output 		.= $after_widget."\n";
			echo $output;
		}
		
		/**
		 * Configuration form.
		 *
		 * @param array $instance
		 * @return void
		 * @since 0.5.0
		 */
		function form( $instance ) 
		{
			$defaults = array(
				'title' 			=> __("Property types", TEST),		
			);
			$instance = wp_parse_args( (array) $instance, $defaults );
			?>				
				<div class="alx-options-posts">
					<p>
						<label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title'); ?>:</label>
						<input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($instance["title"]); ?>" />
					</p>		 
				</div>
			<?php
		}
		
		/**
		 * Display widget.
		 *
		 * @param array $instance
		 * @return array
		 * @since 1.5.6
		 */
		function update( $new_instance, $old_instance )
		{
			$instance = $old_instance;
			$instance['title'] 			= strip_tags($new_instance['title']);		
			return $instance;
		}
	}
	
/*  Register widget
/* ------------------------------------ */
	function properties_register_widget() { 
		register_widget( 'Properties_Widget' );
	}
	add_action( 'widgets_init', 'properties_register_widget' );
	

