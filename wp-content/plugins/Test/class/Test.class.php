<?php
class Test
{
	static $options;
	static $instance;
	static function get_instance()
	{
		if(!static::$instance)
			static::$instance = new static;
		return static::$instance;
	}
	function __construct()
	{
		static::$options							= get_option(__TEST__);
		add_action( 'init',							array(__CLASS__, 'add_test_type'),24 );
		add_action( 'admin_menu', 					array(__CLASS__, 'mbe_add_admin_menus'), 99 );
		add_filter( 'parent_file', 					array(__CLASS__, 'mbe_set_current_menu' ), 1);
		add_action( 'admin_menu', 					array(__CLASS__, 'admin_page_handler'), 9);
		add_action( 'admin_enqueue_scripts', 		array(__CLASS__, 'add_admin_js_script') );		
		add_action( 'wp_enqueue_scripts', 			array(__CLASS__, 'add_frons_js_script'), 99 );
		add_filter( "the_content", 					array(__CLASS__, "the_content"), 10, 2);
		add_action( "init", 						array(__CLASS__, "add_shortcodes"));
		add_filter( 'template_include', 			array(__CLASS__, 'template_include') );
		
		add_filter("manage_edit-".PROPERTY."_columns",			array(__CLASS__, 'add_views_column'), 4);
		add_filter("manage_".PROPERTY."_posts_custom_column",	array(__CLASS__, 'fill_views_column'), 5, 2); 
		add_action("admin_menu",					array(__CLASS__, 'my_extra_fields'));
		add_action("save_post_".PROPERTY,			array(__CLASS__, 'true_save_box_data'));
		
		//AJAX
		if( defined('DOING_AJAX') )
		{
			add_action('wp_ajax_nopriv_myajax',		array(__CLASS__, 'my_action_callback') );
			add_action('wp_ajax_myajax',			array(__CLASS__, 'my_action_callback') );
			add_action('wp_ajax_myajax-admin', 		array(__CLASS__, 'my_action_callback'));
		}
	}
	static function my_action_callback()
	{
		global $camper_event_obj;
		global $var1;
		$nonce = $_POST['nonce'];
		// проверяем nonce код, если проверка не пройдена прерываем обработку
		if ( !wp_verify_nonce( $nonce, 'myajax-nonce' ) ) die ( $_POST['params'][0] );
		
		// обрабатываем данные и возвращаем
		$params				= $_POST['params'];	
		$d					= array( $_POST['params'][0], array() );
				
		switch($params[0])
		{
			case "dod__":				
				$post_id = wp_insert_post( array(
					'post_status'   => 'publish',
					'post_type'     => PROPERTY,
					"post_title"	=> $params[3]
				) );
				wp_set_post_terms( $post_id, array((int)$params[1]), CITY );
				wp_set_post_terms( $post_id, array((int)$params[2]), PROPERTY_TYPE );
				update_post_meta($post_id, "Adress", $params[3]);
				update_post_meta($post_id, "Summae", $params[4]);
				update_post_meta($post_id, "Floor",  $params[5]);
				$d					= array(	
					$params[0],
					array( 
						"text"		=> __($post_id ? "successful create new property. Reload Page" : "Error creating Property", TEST),
					)
				);
				break;
			default:
				do_action("srs_myajax_submit", $params);
				break;
		}
		$d_obj		= json_encode(apply_filters("srs_ajax_data", $d, $params));				
		print $d_obj;
		exit;
	}
	static function template_include( $template )
	{
		if(is_tax(CITY) || is_tax(PROPERTY_TYPE) )
		{
			$template = TEST_REAL_PATH . "template/city.php";
		}
		return $template;
	}
	
	
	static function add_shortcodes()
	{
		add_shortcode('test_first', array(__CLASS__, 'test_first')); 
	}
	static function test_first()
	{
		global $taxonomy_images_plugin;
		$props 		= get_posts("post_type=".PROPERTY."&numberposts=12&post_status=publish");
		
		$cities = get_terms( array(
			'taxonomy'      => array( CITY ), // название таксономии с WP 4.5
			'orderby'       => 'id', 
			'order'         => 'ASC',
			'hide_empty'    => false, 
			'object_ids'    => null, // 
			'include'       => array(),
			'exclude'       => array(), 
			'exclude_tree'  => array(), 
			'number'        => '6', 
			'fields'        => 'all', 
			'count'         => false,
			'slug'          => '', 
			'parent'         => '',
			'hierarchical'  => true, 
			'child_of'      => 0, 
			'get'           => '', // ставим all чтобы получить все термины
			'name__like'    => '',
			'pad_counts'    => false, 
			'offset'        => '', 
			'search'        => '', 
			'cache_domain'  => 'core',
			'name'          => '', // str/arr поле name для получения термина по нему. C 4.2.
			'childless'     => false, // true не получит (пропустит) термины у которых есть дочерние термины. C 4.2.
			'update_term_meta_cache' => true, // подгружать метаданные в кэш
			'meta_query'    => '',
		) );

		
		
		
		
		$html 		= "
		<div class='row props' style=''>
			<div class='container text-center' >" .
			"<h4>".__("Properties", TEST) . "</h4>";
		foreach($props as $prop)
		{
			$thum_url		= get_the_post_thumbnail_url($prop->ID, "full");
			if(!$thum_url)
			{ 
				$thum_url	= TEST_URLPATH ."img/property.png";
			}
			$link			= get_permalink($prop->ID);
			$cits 			= "";
			$prtype 		= "";
			$cur_terms 		= get_the_terms( $prop->ID, CITY );
			foreach( $cur_terms as $cur_term )
			{
				$cits 		.= '<a href="'. get_term_link( (int)$cur_term->term_id, CITY ) .'">'. $cur_term->name .'</a> ';
			}	
			
			$prrs 			= get_the_terms( $prop->ID, PROPERTY_TYPE );
			foreach( $prrs as $cur_term )
			{
				$prtype		.= '<a href="'. get_term_link( (int)$cur_term->term_id, PROPERTY_TYPE ) .'">'. $cur_term->name .'</a>';
			}
			$html 	.= "
			<div class='col-md-3 col-sm-6 col-xs-12 prop'>" . 
				"<a href='$link'><div class='propimg' style='background-image:URL($thum_url);'> </div></a>
				<a href='$link'><div class='prop_title'>" . $prop->post_title. "</div></a>
				<div class='propdata'>".
					"<div>".$cits."</div>".
					get_post_meta($prop->ID, "Adress", true).
					"<div>".$prtype."</div>".
				"</div>".
			"</div>";
		}		
		$html 		.= "
			</div>
		</div>
		<div class='row cities' style=''>
			<div class='container text-center' >" .
			"<h4>".__("Cities", TEST) . "</h4>";
		
		foreach( $cities as $term )
		{
			$cur_bgnd_id	= $taxonomy_images_plugin->settings[$term->term_id];	
			$d 				= wp_get_attachment_image_src($cur_bgnd_id, 'large');
			$cur_bgnd		= $d[0];
			$html 	.= "
			<div class='col-md-2 col-sm-4 col-xs-6'>" . 
				"<a href='". get_term_link( (int)$term->term_id, CITY ) ."'>
					<div class='city' style='background-image:URL($cur_bgnd);'>
						<div class='prop_title'>" . $term->name. "</div>
					</div>
				</a>".
			"</div>";
		}	
			
		
		$html 		.= "
			</div>
		</div>".
			static::get_test_form();	
		return $html;
	}
	static function add_views_column( $columns )
	{
		$cols				= array();
		$cols["cb"]			= " ";
		$cols['image'] 		= __("Image", TEST);
		$cols["title"]		= __("Title");
		$cols['Adress'] 	= __("Adress", TEST);
		$cols['Summae'] 	= __("Summae", TEST);
		$cols['Area'] 		= __("Area", TEST);
		$cols['Floor'] 		= __("Floor", TEST);
		$cols['city'] 		= __("City", TEST);
		$cols['property'] 	= __("Property", TEST);
		return $cols;
	}
	static function fill_views_column($column_name, $post_id)
	{
		switch( $column_name) 
		{		
			case 'city':
				$cur_terms = get_the_terms( $post_id, CITY );
				foreach( $cur_terms as $cur_term ){
					echo '<a href="'. get_term_link( (int)$cur_term->term_id, $cur_term->taxonomy ) .'">'. $cur_term->name .'</a>,';
				}
				break;
			case 'Adress':	
				echo get_post_meta($post_id, "Adress", true);
				break;
			case 'Area':	
				echo get_post_meta($post_id, "Area", true) . __(" m2", TEST);
				break;
			case 'Floor':	
				echo get_post_meta($post_id, "Floor", true);
				break;
			case 'Summae':	
				echo get_post_meta($post_id, "Summae", true) . __(" rub.", TEST);
				break;
			case 'image':	
				$thum_url		= get_the_post_thumbnail_url($id, "full");
				echo "<div style='width:120px; height:120px;background-image:URL($thum_url);background-size:cover;'></div>";
				break;
			case 'property':	
				$cur_terms = get_the_terms( $post_id, PROPERTY_TYPE );
				foreach( $cur_terms as $cur_term ){
					echo '<a href="'. get_term_link( (int)$cur_term->term_id, $cur_term->taxonomy ) .'">'. $cur_term->name .'</a>,';
				}
				break;
			default:
		}
	}
	static function my_extra_fields() 
	{
		add_meta_box( 'extra_fields', __('Parameters', TEST), array(__CLASS__, 'extra_fields_box_func'), PROPERTY, 'normal', 'high'  );
	}
	static function extra_fields_box_func( $post )
	{	
		$enabled	= (int)get_post_meta($post->ID, "enabled", true);
		$html		.= '
		<div>
			<label for="Adress" >'.__("Adress", TEST).'</label><BR>
			<input name="Adress" id="Adress" value="' .  get_post_meta($post->ID, "Adress", true).'" />
		</div><hr/>
		<div>
			<label for="Summae" >'.__("Summae", TEST).'</label><BR>
			<input type="number" name="Summae" id="Summae" value="' .  get_post_meta($post->ID, "Summae", true).'" />
		</div><hr/>
		<div>
			<label for="Area" >'.__("Area", TEST).'</label><BR>
			<input type="number" name="Area" id="Area" value="' .  get_post_meta($post->ID, "Area", true).'" />
		</div><hr/>
		<div>
			<label for="Floor" >'.__("Floor", TEST).'</label><BR>
			<input type="number" name="Floor" id="Floor" value="' .  get_post_meta($post->ID, "Floor", true).'" />
		</div><hr/>';
		echo $html;
		wp_nonce_field( basename( __FILE__ ), 'prop_metabox_nonce' );
	}
	static function true_save_box_data ( $post_id ) 
	{
		// проверяем, пришёл ли запрос со страницы с метабоксом
		if ( !isset( $_POST['prop_metabox_nonce'] )
		|| !wp_verify_nonce( $_POST['prop_metabox_nonce'], basename( __FILE__ ) ) )
			return $post_id;
		// проверяем, является ли запрос автосохранением
		if ( defined('DOING_AUTOSAVE') && DOING_AUTOSAVE ) 
			return $post_id;
		// проверяем, права пользователя, может ли он редактировать записи
		if ( !current_user_can( 'edit_post', $post_id ) )
			return $post_id;
		
		//wp_die("222");
		update_post_meta($post_id, 'Adress', 	($_POST['Adress']) );	
		update_post_meta($post_id, 'Summae', 	($_POST['Summae']) );	
		update_post_meta($post_id, 'Area', 		($_POST['Area']) );		
		update_post_meta($post_id, 'Floor', 	($_POST['Floor']) );		
		return $post_id;
	}
	
	
	static function admin_page_handler()
	{
		add_menu_page( 
			__("Test plugin", TEST),
			__("Test plugin", TEST),
			'manage_options',
			'nc_sdelka_page',
			array(__CLASS__, 'sdelka_setting_pages'),
			"", //SDELKA_URLPATH . 'img/little_logo.png',
			'3'
		);
	}
	static function sdelka_setting_pages()
	{
		
		if($_POST['save'])
		{
			static::$options['users']				 	= $_POST['users'];
			static::$options['telephone']				= $_POST['telephone'];
			update_option(__TEST__, static::$options);				
		}
		$html = "<h1>" . __("Settings")."</h1>" . 
		'<form name="settings"  method="post"  enctype="multipart/form-data">
		<ul class="nav nav-tabs">
			<li class="active"><a data-toggle="tab" href="#panel1">' . __("Basic Settings", TEST) . '</a></li>
			<li><a data-toggle="tab" href="#panel2">' . __("Landing page", TEST) . '</a></li>
			<!--li><a data-toggle="tab" href="#panel5">' . __("About us page", TEST) . '</a></li>
			<li><a data-toggle="tab" href="#panel3">' . __("Special functions", TEST) . '</a></li-->
				
			</ul>
			 
			<div class="tab-content">
			  <div id="panel1" class="tab-pane fade in active ">
				<h3>' . __("Basic Settings", TEST) . '</h3>
				<div class=container>
					<div class="row space _animate" data-animate=fadeIn>
						<div class="col-md-4 col-sm-6 col-xs-12">
							<board title>'.
								__("Telephone", TEST) .
							'</board_title>
							<input name="telephone" style="width:320px;" value="'. static::$options['telephone'].'"/> 
							<p>
						</div>
					</div>
				</div>
			  </div>
			  <div id="panel2" class="tab-pane fade">
				<h3>' . __("Landing page", TEST) . '</h3>
				
				<h4>' . __("Contents", TEST). '</h4>
				<div class="panel-group" id="accordion">
				  <!-- 1 панель -->
				  <div class="panel panel-default">					
						<div class="panel-heading">
						  <h4 class="panel-title">
							<a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">' . sprintf(__("%s slide", TEST), "1") . '</a>
						  </h4>
						</div>
						<div id="collapseOne" class="panel-collapse collapse in">
							<div class="panel-body">'.
								
							'</div>
						</div>
				  </div>				  
				  <!-- 2 панель -->
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">' . sprintf(__("%s slide", TEST), "2") . '</a>
					  </h4>
					</div>
					<div id="collapseTwo" class="panel-collapse collapse">
						<div class="panel-body">'.
							
						'</div>
					</div>
				  </div>
				  <!-- 3 панель -->
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">' . sprintf(__("%s slide", TEST), "3") . '</a>
					  </h4>
					</div>
					<div id="collapseThree" class="panel-collapse collapse">
					  <div class="panel-body">'.
							
						'</div>
					</div>
				  </div>
				  <!-- 4 панель -->
				  <div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapse4">' . sprintf(__("%s slide", TEST), "4") . '</a>
					  </h4>
					</div>
					<div id="collapse4" class="panel-collapse collapse">
					  <div class="panel-body">'.
							
						'</div>
					</div>
				  </div>
				</div>
				
				
			  </div>
			  <div id="panel3" class="tab-pane fade">
				<div class="panel panel-padding">
					<textarea rows="20" style="width:100%;">'.
						serialize(get_option("pc_data")) .
					'</textarea>
				</div>
			  </div>
			  <div id="panel5" class="tab-pane fade">
				<div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">' . sprintf(__("%s persone", TEST), "1") . '</a>
					  </h4>
					</div>					
					<div class="panel-body">'.
						//static::get_user(0, true) .
					'</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">' . sprintf(__("%s persone", TEST), "2") . '</a>
					  </h4>
					</div>					
					<div class="panel-body">'.
						//static::get_user(1, true) .
					'</div>
				</div>
				<div class="panel panel-default">
					<div class="panel-heading">
					  <h4 class="panel-title">
						<a data-toggle="collapse" data-parent="#accordion" href="#collapseThree">' . sprintf(__("%s persone", TEST), "3") . '</a>
					  </h4>
					</div>					
					<div class="panel-body">'.
						//static::get_user(2, true) .
					'</div>
				</div>
			  </div>
			</div>
			<div>
				<input type=submit name=save class="btn btn-primary" value="' . __("Change") .'"/>
			</div>
			</form>';
		
		echo $html;
	}
	
	static function add_test_type()
	{
		
		register_taxonomy(
			PROPERTY_TYPE, 
			array( ), 
			array(
				'label'             => '', 
				'labels'            => array(
					'name'              => __('Property type', TEST),
					'singular_name'     => __('Property type', TEST),
					'search_items'      => __('Search Property type', TEST),
					'all_items'         => __('All Property type', TEST),
					'view_item '        => __('View Property type', TEST),
					'parent_item'       => __('Parent Property type', TEST),
					'parent_item_colon' => __('Parent Property type:', TEST),
					'edit_item'         => __('Edit Property type', TEST),
					'update_item'       => __('Update Property type', TEST),
					'add_new_item'      => __('Add New Property type', TEST),
					'new_item_name'     => __('New Property type Name', TEST),
					'menu_name'         => __('Property type', TEST),
				),
				'description'           => '',
				'public'                => true,
				'publicly_queryable'    => null,
				'show_in_nav_menus'     => true,
				'show_ui'               => true,
				'show_tagcloud'         => true,
				'show_in_rest'          => null,
				'rest_base'             => null,
				'hierarchical'          => false,
				'update_count_callback' => '',
				'rewrite'               => true,
				'capabilities'          => array(),
				'meta_box_cb'           => null, 
				'show_admin_column'     => false,
				'_builtin'              => false,
				'show_in_quick_edit'    => null,
			) 
		);
		register_taxonomy(
			CITY, 
			array( ), 
			array(
				'label'             => '', 
				'labels'            => array(
					'name'              => __('City', TEST),
					'singular_name'     => __('City', TEST),
					'search_items'      => __('Search City', TEST),
					'all_items'         => __('All Cities', TEST),
					'view_item '        => __('View City', TEST),
					'parent_item'       => __('Parent City', TEST),
					'parent_item_colon' => __('Parent City:', TEST),
					'edit_item'         => __('Edit City', TEST),
					'update_item'       => __('Update City', TEST),
					'add_new_item'      => __('Add New City', TEST),
					'new_item_name'     => __('New City Name', TEST),
					'menu_name'         => __('City', TEST),
				),
				'description'           => '',
				'public'                => true,
				'publicly_queryable'    => null,
				'show_in_nav_menus'     => true,
				'show_ui'               => true,
				'show_tagcloud'         => true,
				'show_in_rest'          => null,
				'rest_base'             => null,
				'hierarchical'          => false,
				'update_count_callback' => '',
				'rewrite'               => true,
				'capabilities'          => array(),
				'meta_box_cb'           => null, 
				'show_admin_column'     => false,
				'_builtin'              => false,
				'show_in_quick_edit'    => null,
			) 
		);
		
		$labels = array(
			'name' => __('Property', TEST),
			'singular_name' => __("Property", TEST), // админ панель Добавить->Функцию
			'add_new' => __("add Property", TEST),
			'add_new_item' => __("add new Property", TEST), // заголовок тега <title>
			'edit_item' => __("edit Property", TEST),
			'new_item' => __("new Property",TEST),
			'all_items' => __("all Properties", TEST),
			'view_item' => __("view Property", TEST),
			'search_items' => __("search Property", TEST),
			'not_found' =>  __("Property not found", TEST),
			'not_found_in_trash' => __("no found Property in trash", TEST),
			'menu_name' => __("Contacts", TEST) // ссылка в меню в админке
		);
		$args = array(
			'labels' => $labels,
			'description' => __('Property',TEST),
			'exclude_from_search' =>true, 
			'public' => true,
			'show_ui' => true, // показывать интерфейс в админке
			'has_archive' => true, 
			'menu_icon' =>'dashicons-admin-site', //SMC_URLPATH .'/img/pin.png', // иконка в меню
			'menu_position' => 20, // порядок в меню			
			'supports' => array( 'title', 'editor', 'author', "thumbnail")
			,'show_in_nav_menus' => true
			,'show_in_menu' => "nc_sdelka_page"
			,'taxonomies' => array( PROPERTY_TYPE, CITY )
			,'capability_type' => 'page'
			,'capability_type' => 'post'
		);
		register_post_type(PROPERTY, $args);
	}
	static function mbe_add_admin_menus() 
	{
		add_submenu_page( 'nc_sdelka_page', __('All Property types', TEST), __('All Property types', TEST), 'manage_options', 'edit-tags.php?taxonomy='.PROPERTY_TYPE);
		add_submenu_page( 'nc_sdelka_page', __('All Cities', TEST), __('All Cities', TEST), 'manage_options', 'edit-tags.php?taxonomy='.CITY);
    }
	static function mbe_set_current_menu( $parent_file ) 
	{
        global $current_screen;
		$taxonomy = $current_screen->taxonomy;
		if ( $taxonomy == PROPERTY_TYPE || $taxonomy == CITY )
			$parent_file = 'nc_sdelka_page';
		return $parent_file;
    }
	
	
	static function add_frons_js_script()
	{
		wp_register_style("front", TEST_URLPATH . "css/front.css", array());
		wp_enqueue_style( "front");	
		wp_register_script("front", plugins_url( '../js/front.js', __FILE__ ));
		wp_enqueue_script("front");		
		
		//ajax
		wp_localize_script( 'jquery', 'myajax', array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('myajax-nonce')
		));	
	}
	static function add_admin_js_script()
	{	
		wp_register_style("bootstrap", TEST_URLPATH . "css/bootstrap.min.css", array());
		wp_enqueue_style( "bootstrap");	
		wp_register_script("bootstrap", plugins_url( '../js/bootstrap.min.js', __FILE__ ));
		wp_enqueue_script("bootstrap");		
		return;	
				
		//ajax
		wp_localize_script( 'jquery', 'myajax', array(
			'url' => admin_url('admin-ajax.php'),
			'nonce' => wp_create_nonce('myajax-nonce')
		));
	}
	
	static function the_content($content)
	{
		global $post;
		if( is_front_page() )
		{
			//
		}
		if($post->post_type == PROPERTY)
		{
			
			$text 	= "
			<div class='container'>
				<div class='row'></div>";
			
			$City = "";
			$cur_terms = get_the_terms( $post_id, CITY );
			foreach( $cur_terms as $cur_term )
			{
				$City .= '<a href="'. get_term_link( (int)$cur_term->term_id, $cur_term->taxonomy ) .'">'. $cur_term->name .'</a>,';
			}
			
			$Prop = "";
			$cur_terms = get_the_terms( $post_id, PROPERTY_TYPE );
			foreach( $cur_terms as $cur_term )
			{
				$Prop .= '<a href="'. get_term_link( (int)$cur_term->term_id, $cur_term->taxonomy ) .'">'. $cur_term->name .'</a>,';
			}
			$Adress = get_post_meta($post->ID, "Adress", true);	
			$Summae = get_post_meta($post->ID, 'Summae', true);	
			$Area 	= get_post_meta($post->ID, 'Area', true);		
			$Floor 	= get_post_meta($post->ID, 'Floor', true);	
			$text 	.= "
			<div class='row'>
				<div class='col-md-3 col-sm-6 col-xs-12 text-right'>".__("City", TEST) . "</div>
				<div class='col-md-9 col-sm-6 col-xs-12'>" . $City . "</div>
			</div>";		
			$text 	.= "
			<div class='row'>
				<div class='col-md-3 col-sm-6 col-xs-12 text-right'>".__("Property type", TEST) . "</div>
				<div class='col-md-9 col-sm-6 col-xs-12'>" . $Prop . "</div>
			</div>";		
			$text 	.= "
			<div class='row'>
				<div class='col-md-3 col-sm-6 col-xs-12 text-right'>".__("Adress", TEST) . "</div>
				<div class='col-md-9 col-sm-6 col-xs-12'>" . $Adress . "</div>
			</div>";		
			$text 	.= "
			<div class='row'>
				<div class='col-md-3 col-sm-6 col-xs-12 text-right'>".__("Summae", TEST) . "</div>
				<div class='col-md-9 col-sm-6 col-xs-12'>" . $Summae . " " . __("rubl.", TEST)  . "</div>
			</div>";				
			$text 	.= "
			<div class='row'>
				<div class='col-md-3 col-sm-6 col-xs-12 text-right'>".__("Area", TEST) . "</div>
				<div class='col-md-9 col-sm-6 col-xs-12'>" . $Area . " " . __("m2", TEST) . "</div>
			</div>";			
			$text 	.= "
			<div class='row'>
				<div class='col-md-3 col-sm-6 col-xs-12 text-right'>".__("Floor", TEST) . "</div>
				<div class='col-md-9 col-sm-6 col-xs-12'>" . $Floor . "</div>
			</div>";		
			$text 	.= "
			</div>";		
			return $text;	
		}
		return $content;
	}
	static function get_test_form()
	{
		$html = "
		<div class='row' style='background-color:#999; padding:40px 20px;'>
			<div class='container text-center'>
				<div class='col-md-12'>
					<div class='testtitle'>".__("Set new Property", TEST)."</div>
				</div>
				".
				'<div class="form-horizontal">
				
					<div class="form-group has-feedback col-md-12">
						<label for="City" class="control-label col-md-3">' . __("City", TEST) . '</label>
						<div class="col-md-6">'.
							static::wp_dropdown( array( "term" => CITY, "class" => "form-control city__" )).
						'</div>
					</div>
					
					<div class="form-group has-feedback col-md-12">
						<label for="Adress" class="control-label col-md-3">' . __("Adress", TEST) . '</label>
						<div class="col-md-6">
							<div class="input-group">        
								<input type="text" class="form-control adress__" name="Adress" pattern="[A-Za-z]{6,}">
							</div>
							<span class="glyphicon form-control-feedback"></span>
						</div>
					</div>
					
					<div class="form-group has-feedback col-md-12">
						<label for="Summae" class="control-label col-md-3">' . __("Summae", TEST) . '</label>
						<div class="col-md-6">
							<div class="input-group">
								<input type="number" class="form-control summae__" name="Summae" min="0">
							</div>
						</div>
					</div>
					
					<div class="form-group has-feedback col-md-12">
						<label for="Floor" class="control-label col-md-3">' . __("Floor", TEST) . '</label>
						<div class="col-md-6">
							<div class="input-group">
								<input type="number" class="form-control floor__" name="Floor" min="0">
							</div>
						</div>
					</div>					
					
				
					<div class="form-group has-feedback col-md-12">
						<label for="pt" class="control-label col-md-3">' . __("Property Type", TEST) . '</label>
						<div class="col-md-6">'.
							static::wp_dropdown( array( "term" => PROPERTY_TYPE, "class" => "form-control prop__", "id" => "pt" )).
						'</div>
					</div>
					
					<div class=" send_new btn btn-lg btn-success" data-toggle="modal">
						' . __("Set new Property", TEST) . '
					</div>
				 </div>'.
				"
			</div>
		</div>";
		return $html;
	}
	
	static function wp_dropdown($params="-1")
	{
		if(!is_array($params))
			$params	= array();
		if($params['post_type'])
		{
			$hubs	= get_posts(
				array(
					"post_type" 	=> $params['post_type'],
					"numberposts" 	=> -1,
					"post_status"	=> "publish"
				)
			);
		}
		if($params['term'])
		{
			$args = array(
				'taxonomy'      => $params['term'], 
				'orderby'       => 'name', 
				'order'         => 'ASC',
				'hide_empty'    => false, 
				'hierarchical'  => true,
				"default"		=> -1
			); 
			if(isset($params["parent"]))
			{
				$args["parent"] = $params["parent"];
			}
			$hubs	= array();
			$terms	= get_terms( $args );
			foreach($terms as $term)
			{
				$hub = new stdClass();
				$hub->post_title	= $term->name;
				$hub->ID			= $term->term_id;
				$hubs[]	= $hub;
			}
		}
		if(is_array($params['hubs']))
		{
			$hubs = $params['hubs'];
		}
		//$hubs		= self::get_all($params['args']);
		$html		= "<select ";
		if($params['class'])
			$html	.= "class='".$params['class']."' ";
		if($params['style'])
			$html	.= "style='".$params['style']."' ";
		if($params['name'])
			$html	.= "name='".$params['name']."' ";
		if($params['placeholder'])
			$html	.= "placeholder='".$params['placeholder']."' ";
		if($params['id'])
			$html	.= "id='".$params['id']."' ";
		if($params['multi'])
			$html	.= " multiple  ";
		$html		.= " >";
		$html		.= "<option value='" . $params['default'] . "'>---</option>";			
		foreach($hubs as $hub)
		{
			$sel 	= is_array($params['selected']) 
				? 
					in_array($hub->ID, $params['selected']) ? " selected " : ""
				:
					selected($hub->ID, $params['selected'], false);
			$html	.= "<option value='".$hub->ID."' $sel>" . $hub->post_title . "</option>";
		}
		$html		.= "</select>";
		return $html;	
	}
}