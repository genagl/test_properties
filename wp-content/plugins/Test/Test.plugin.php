<?php
/*
Plugin Name: Test
Plugin URI: http://glazun.com/?page_id=103
Description: Content of site for Test
Version: 1.0.0
Date: 1.04.2017
Author: Genagl
Author URI: http://www.glazun.com/wordpress
License: GPL2
Text Domain:   test
Domain Path:   /lang/
*/
/*  Copyright 2014  Genagl  (email: genag1@list.ru)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful, 
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
*/ 
//библиотека переводов
function init_test_textdomain() 
{ 
	if (function_exists('load_plugin_textdomain')) 
		$do	= load_plugin_textdomain("test", false , dirname( plugin_basename( __FILE__ ) ) .'/lang/'); 
} 
// Вызываем ее до начала выполнения плагина 
add_action('plugins_loaded', 'init_test_textdomain');

define('TEST_URLPATH', WP_PLUGIN_URL . '/Test/');
define('TEST_REAL_PATH', WP_PLUGIN_DIR . '/' . plugin_basename(dirname(__FILE__)) . '/');
define('__TEST__', 'test');
define('PROPERTY', 'property');
define('PROPERTY_TYPE', 'property_type');
define('CITY', 'city');

require_once( TEST_REAL_PATH . 'class/Test.class.php' );
require_once( TEST_REAL_PATH . 'widget/Properties.widget.php' );
require_once( TEST_REAL_PATH . 'widget/Cities.widget.php' );

$test_class = Test::get_instance();
